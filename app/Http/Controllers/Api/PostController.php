<?php

namespace App\Http\Controllers\Api;

use Response;
use Illuminate\Http\Request;
use App\Events\NewPostAdded;
use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\Tag\TagRepositoryInterface;

/**
 * Class PostController
 * @package App\Http\Controllers
 */
class PostController extends Controller
{
    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * @var TagRepositoryInterface
     */
    protected $tagRepository;

    /**
     * PostController constructor.
     *
     * @param PostRepositoryInterface $postRepository
     */
    public function __construct(PostRepositoryInterface $postRepository, TagRepositoryInterface $tagRepository)
    {
        parent::__construct();

        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * Get all posts
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $params = $request->only('tags');

        if (!empty($params['tags'])) {
            $tags = explode(',', $params['tags']);
            $posts = $this->postRepository->getByTags($tags);
        } else {
            $posts = $this->postRepository->getAll();
        }

        $this->response['data']['data'] = array('posts' => $posts);

        return Response::json($this->response['data'], $this->response['code']);
    }

    /**
     * Count all posts by tags (Optional)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function count(Request $request)
    {
        $params = $request->only('tags');

        if (!empty($params['tags'])) {
            $tags = explode(',', $params['tags']);
            $count = $this->postRepository->countByTags($tags);
        } else {
            $count = $this->postRepository->countAll();
        }

        $this->response['data']['data'] = array('total_posts' => $count);

        return Response::json($this->response['data'], $this->response['code']);
    }

    /**
     * Add a new post
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $params = $request->only('title', 'author', 'body');

        $rules = array(
            'title' => 'required|min:2',
            'author' => 'required|min:2',
            'body' => 'required|min:6'
        );

        // In case of error it will throw an exception and will be handled with Json error response
        $this->validate($request, $rules);

        $this->postRepository->insert($params);

        \Event::fire(new NewPostAdded($params));

        return Response::json($this->response['data'], $this->response['code']);
    }


    /**
     * Update an existing post by post id
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $params = $request->only('title', 'author', 'body');

        $rules = array(
            'title' => 'required|min:2',
            'author' => 'required|min:2',
            'body' => 'required|min:6'
        );

        // In case of error it will throw an exception and will be handled with Json error response
        $this->validate($request, $rules);

        $this->postRepository->update($id, $params);

        return Response::json($this->response['data'], $this->response['code']);
    }

    /**
     * Delete an existing post by post id
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        // In case of error it will throw an exception and will be handled with Json error response
        $this->postRepository->delete($id);

        return Response::json($this->response['data'], $this->response['code']);
    }

    /**
     * Add tag to existing post
     *
     * @param  int $id
     * @param  int $tagId
     * @return \Illuminate\Http\JsonResponse
     */
    public function addTag($id, $tagId)
    {
        // In case of error it will throw an exception and will be handled with Json error response
        $this->tagRepository->find($tagId);
        $this->postRepository->addTag($id, $tagId);

        return Response::json($this->response['data'], $this->response['code']);
    }

    /**
     * Remove tag from the post
     *
     * @param  int $id
     * @param  int $tagId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTag($id, $tagId)
    {
        // In case of error it will throw an exception and will be handled with Json error response
        $this->tagRepository->find($tagId);
        $this->postRepository->deleteTag($id, $tagId);

        return Response::json($this->response['data'], $this->response['code']);
    }
}
