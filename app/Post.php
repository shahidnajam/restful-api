<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'author', 'body'];

    /**
     * The posts that belong to the tag.
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps()->withPivot(array('post_id'))->select(array('id', 'name'));
    }
}
