<?php

namespace App\Repositories\Post;

/**
 * Interface PostRepositoryInterface
 * @package App\Repositories\Post
 */
interface PostRepositoryInterface
{
    /**
     * Get all posts
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll();

    /**
     * Get all posts by tags
     *
     * @param array $tags
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getByTags(array $tags);

    /**
     * Count all posts
     *
     * @param array $tags
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function countAll();

    /**
     * Count all posts by tag or tags
     *
     * @param array $tags
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function countByTags(array $tags);

    /**
     * Insert a new post
     *
     * @param array $data
     * @return mixed
     */
    public function insert(array $data);

    /**
     * Update post by an id
     *
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * Delete post by an id
     *
     * @param  int $id
     * @return mixed
     */
    public function delete($id);

    /**
     * Add tag to post
     *
     * @param int $id
     * @param int $tagId
     * @return mixed
     */
    public function addTag($id, $tagId);


    /**
     * Delete or remove the tag from the post
     *
     * @param int $id
     * @param int $tagId
     * @return mixed
     */
    public function deleteTag($id, $tagId);
}