<?php

namespace App\Listeners;

use App\Events\NewPostAdded;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class NewPostEmail
 * @package App\Listeners
 */
class NewPostEmail implements ShouldQueue
{
    /**
     * @var Mailer
     */
    protected $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  NewPostAdded  $event
     * @return void
     */
    public function handle(NewPostAdded $event)
    {
        $this->mailer->send('emails.newpost', ['data' => $event->data ], function ($m) {
            $config = config('task.mail');
            $m->from($config['from']['address'], $config['from']['name']);
            $m->to($config['to']['address'], $config['to']['name'])->subject('New post added');
        });
    }
}
