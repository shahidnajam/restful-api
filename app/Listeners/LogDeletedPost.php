<?php

namespace App\Listeners;

use App\Events\PostDeleted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

/**
 * Class NewPostEmail
 * @package App\Listeners
 */
class LogDeletedPost
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  NewPostAdded $event
     * @return void
     */
    public function handle(PostDeleted $event)
    {
        Log::info('Post has been deleted', array('data' => $event->data));
    }
}
