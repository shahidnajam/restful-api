<?php

namespace App\Providers;

use App\Post;
use App\Tag;
use App\Repositories\Post\PostRepository;
use App\Repositories\Tag\TagRepository;
use App\Services\Cache\LaravelCache;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Post\PostRepositoryInterface', function ($app) {
            return new PostRepository(
                new Post,
                new LaravelCache($this->app['cache'], ['post'])
            );
        });

        $this->app->bind('App\Repositories\Tag\TagRepositoryInterface', function ($app) {
            return new TagRepository(
                new Tag,
                new LaravelCache($this->app['cache'], ['post', 'tag'])
            );
        });
    }
}
