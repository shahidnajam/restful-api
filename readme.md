# RESTful API sample task  with CRUD access to Post and tags

### Description
REST full api CRUD access to two resources without any security check:
- Post (with fields like: title, body and so on)
- Tags (with only name field) many-to-may relation with posts

Additional methods for API:
- select all posts by tag or tags
- count posts by tag or tags

After any post created system should send email to specified in configuration file Email.
 
After any post removed system should log information about it.

### Technologies used for this task
- Laravel Framework 5.2
- PHP 5.6
- MySQL
- Memcached
- Beanstalkd
- Swagger UI to visualize RESTFUL API (Frontend part)
- PHPUnit with Mockery
- Composer
