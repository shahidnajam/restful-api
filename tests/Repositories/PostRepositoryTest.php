<?php

use Mockery as m;
use App\Repositories\Post\PostRepository;

/**
 * Class PostRepositoryTest
 */
class PostRepositoryTest extends TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    protected $model;

    /**
     * @var \Mockery\MockInterface
     */
    protected $cache;
    /**
     * @var \App\Repositories\Post\PostRepositoryInterface
     */
    protected $repository;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();

        $this->cache = m::mock(\App\Services\Cache\LaravelCache::class);
        $this->model = m::mock(\App\Post::class);

        $this->repository = new PostRepository($this->model, $this->cache);
    }

    /**
     *
     */
    public function tearDown()
    {
        m::close();
    }

    /**
     * Check the getAll() method when data is not cached
     */
    public function testGetAllDb()
    {
        $this->cache->shouldReceive('get')->once()->AndReturnNull();
        $this->cache->shouldReceive('put')->once();

        $post = [
            'id' => 4,
            'title' => 'Test title',
            'author' => 'shahid',
            'body' => 'this is test post',
            'created_at' => '2016-08-12 10:16:16',
            'updated_at' => '2016-08-12 10:16:17',
            'tags' => [
                'id' => 1,
                'name' => 'tagtest'
            ]
        ];

        $this->model->shouldReceive('with')->once()->with('tags')->AndReturn(m::self());
        $this->model->shouldReceive('get')->once()->AndReturn($post);

        $this->assertEquals($post, $this->repository->getAll());
    }

    /**
     * Check the getAll() method when data is already cached
     */
    public function testGetAllCached()
    {
        $cachedPost = [
            'id' => 4,
            'title' => 'Test title',
            'author' => 'shahid',
            'body' => 'this is test post',
            'created_at' => '2016-08-12 10:16:16',
            'updated_at' => '2016-08-12 10:16:17',
            'tags' => [
                'id' => 1,
                'name' => 'tagtest'
            ]
        ];

        $this->cache->shouldReceive('get')->once()->AndReturn($cachedPost);

        $this->model->shouldNotReceive('get');

        $this->assertEquals($cachedPost, $this->repository->getAll());
    }


    /**
     *
     */
    public function testCountAll()
    {
        $this->cache->shouldReceive('get')->once()->AndReturnNull();
        $this->cache->shouldReceive('put')->once();

        $this->model->shouldReceive('with')->once()->with('tags')->AndReturn(m::self());
        $this->model->shouldReceive('get')->once()->AndReturn(m::self());

        $testCount = 5;
        $this->model->shouldReceive('count')->once()->AndReturn($testCount);

        $this->assertEquals($testCount, $this->repository->countAll());
    }

    /**
     *
     */
    public function testCountWithTags()
    {
        $this->cache->shouldReceive('get')->once()->AndReturnNull();
        $this->cache->shouldReceive('put')->once();

        $this->model->shouldReceive('with')->once()->with('tags')->AndReturn(m::self());
        $this->model->shouldReceive('whereHas')->once()->AndReturn(m::self());
        $this->model->shouldReceive('get')->once()->AndReturn(m::self());

        $testCount = 5;
        $this->model->shouldReceive('count')->once()->AndReturn($testCount);

        $this->assertEquals($testCount, $this->repository->countByTags(array('java')));
    }

    /**
     * Check the getByTags() method when data is not cached
     */
    public function testGetByTagsDb()
    {
        $this->cache->shouldReceive('get')->once()->AndReturnNull();
        $this->cache->shouldReceive('put')->once();

        $this->model->shouldReceive('with')->once()->with('tags')->AndReturn(m::self());

        $this->model->shouldReceive('whereHas')->once()->with('tags', m::on(function ($closure) {
            $query = m::mock(\App\Post::class);
            $query->shouldReceive('whereIn')->once()->with('name', array('tagtest'))->andReturn(m::self());
            $closure($query);
            return true;
        }))->once()->AndReturn(m::self());

        $post = [
            'id' => 4,
            'title' => 'Test title',
            'author' => 'shahid',
            'body' => 'this is test post',
            'created_at' => '2016-08-12 10:16:16',
            'updated_at' => '2016-08-12 10:16:17',
            'tags' => [
                'id' => 1,
                'name' => 'tagtest'
            ]
        ];

        $this->model->shouldReceive('get')->once()->AndReturn($post);

        // PostRepository::getAll depends on Elequent model, it should be same as model get result
        $this->assertEquals($post, $this->repository->getByTags(array('tagtest')));
    }

    /**
     * Check the getByTags() method when data is cached
     */
    public function testGetByTagsCached()
    {
        $post = [
            'id' => 4,
            'title' => 'Test title',
            'author' => 'shahid',
            'body' => 'this is test post',
            'created_at' => '2016-08-12 10:16:16',
            'updated_at' => '2016-08-12 10:16:17',
            'tags' => [
                'id' => 1,
                'name' => 'tagtest'
            ]
        ];

        $this->cache->shouldReceive('get')->once()->AndReturn($post);

        $this->model->shouldNotReceive('get');

        // PostRepository::getAll depends on Elequent model, it should be same as model get result
        $this->assertEquals($post, $this->repository->getByTags(array('tagtest')));
    }

    /**
     * Test insert()
     */
    public function testInsert()
    {
        $inputData = ['title' => 'New test title', 'author' => 'shahid'];

        $this->model->shouldReceive('create')->once()->with($inputData)->andReturn(true);
        $this->cache->shouldReceive('flush')->once();

        $this->assertTrue($this->repository->insert($inputData));
    }

    /**
     * Test update()
     */
    public function testUpdate()
    {
        $inputId = 1;
        $inputData = ['title' => 'New test title'];

        $this->model->shouldReceive('findOrFail')->once()->with($inputId)->AndReturn(m::self());
        $this->model->shouldReceive('update')->once()->with($inputData)->andReturn(true);
        $this->cache->shouldReceive('flush')->once();

        $this->repository->update($inputId, $inputData);
    }

    /**
     * Test delete()
     */
    public function testDelete()
    {
        $inputId = 3;

        $this->model->shouldReceive('findOrFail')->once()->with($inputId)->AndReturn(m::self());
        $this->model->shouldReceive('delete')->once()->andReturn(true);
        $this->cache->shouldReceive('flush')->once();
        \Event::shouldReceive('fire')->once();

        $this->repository->delete($inputId);
    }

    /**
     * Test addTag()
     */
    public function testAddTag()
    {
        $inputId = 3;
        $inputTagId = 5;

        $this->model->shouldReceive('findOrFail')->with($inputId)->AndReturn(m::self());
        $this->model->shouldReceive('getAttribute')->andReturn(m::self());

        // Case if tag not exists already then attach
        $this->model->shouldReceive('contains')->once()->andReturn(false);
        $this->model->shouldReceive('tags')->once()->andReturn(m::self());
        $this->model->shouldReceive('attach')->once();
        $this->cache->shouldReceive('flush')->once();
        $this->repository->addTag($inputId, $inputTagId);

        // Case if tag exists already then don't attach
        $this->model->shouldReceive('contains')->once()->andReturn(true);
        $this->model->shouldReceive('attach')->never();
        $this->repository->addTag($inputId, $inputTagId);
    }

    /**
     * Test deleteTag()
     */
    public function testDeleteTag()
    {
        $inputId = 3;
        $inputTagId = 5;

        $this->model->shouldReceive('findOrFail')->once()->with($inputId)->AndReturn(m::self());
        $this->model->shouldReceive('tags')->once()->andReturn(m::self());
        $this->model->shouldReceive('detach')->once();
        $this->cache->shouldReceive('flush')->once();

        $this->repository->deleteTag($inputId, $inputTagId);
    }
}
